-- TODO:
-- look into window scaling more

_, _, flags = love.window.getMode()
width, height = love.window.getDesktopDimensions(flags.display) -- get the width and heght in pixels

window = {translateX = 0, translateY = 0, scale = 1, width = width, height = height}

love.window.setMode(width, height, {resizable=false, borderless=false, fullscreen=true})

function love.load()
  resize(width, height)

  DirectionQueue = { 'right' }
  Timer = 0
  Score = 0

  -- game board
  GridXCount = window.width / 5
  GridYCount = window.height / 5

  -- food position
  function MoveFood()
    local possibleFoodPositions = {}

    for foodX = 1, GridXCount do
      for foodY = 1, GridYCount do
        local possible = true

        for segmentIndex, segment in ipairs(SnakeSegments) do
          if foodX == segment.x and foodY == segment.y then
            possible = false
          end
        end

        if possible then
          table.insert(possibleFoodPositions, { x = foodX, y = foodY })
        end
      end
    end

    FoodPosition = possibleFoodPositions[love.math.random(#possibleFoodPositions)]
  end

  function Reset()
    -- snake
    SnakeAlive = true
    SnakeSegments = {
      { x = 3, y = 1 },
      { x = 2, y = 1 },
      { x = 1, y = 1 },
    }
    DirectionQueue = { 'right' }
    MoveFood()
    Score = 0
  end

  function UpdateScore()
    Score = Score + 1
  end

  Reset()
end

function love.update(dt)
  Timer = Timer + dt
  if SnakeAlive then
    if Timer >= 0.15 then
      Timer = 0
      -- print('tick')

      -- Snake Movement
      if #DirectionQueue > 1 then
        table.remove(DirectionQueue, 1)
      end

      local nextXPosition = SnakeSegments[1].x
      local nextYPosition = SnakeSegments[1].y

      if DirectionQueue[1] == 'right' then
        nextXPosition = nextXPosition + 1
        if nextXPosition > GridXCount then
          nextXPosition = 1
        end
      elseif DirectionQueue[1] == 'left' then
        nextXPosition = nextXPosition - 1
        if nextXPosition < 1 then
          nextXPosition = GridXCount
        end
      elseif DirectionQueue[1] == 'down' then
        nextYPosition = nextYPosition + 1
        if nextYPosition > GridYCount then
          nextYPosition = 1
        end
      elseif DirectionQueue[1] == 'up' then
        nextYPosition = nextYPosition - 1
        if nextYPosition < 1 then
          nextYPosition = GridYCount
        end
      end

      -- check to see if any segment except for the last
      -- is at the same position as the head position
      -- if so the the snake crashed into it self
      local canMove = true

      for segmentIndex, segment in ipairs(SnakeSegments) do
        if segmentIndex ~= #SnakeSegments
            and nextXPosition == segment.x
            and nextYPosition == segment.y then
          canMove = false
        end
      end

      if canMove then
        table.insert(SnakeSegments, 1, {
          x = nextXPosition, y = nextYPosition
        })
        -- Eating Food
        if SnakeSegments[1].x == FoodPosition.x
            and SnakeSegments[1].y == FoodPosition.y then
          UpdateScore()
          MoveFood()
        else
          table.remove(SnakeSegments)
        end
      else
        SnakeAlive = false
      end
    end
    -- reload if dead
  elseif Timer >= 2 then
    Reset()
  end
end

-- controls
function love.keypressed(key)
  if key == 'right'
      and DirectionQueue[#DirectionQueue] ~= 'right'
      and DirectionQueue[#DirectionQueue] ~= 'left' then
    table.insert(DirectionQueue, 'right')
  elseif key == 'left'
      and DirectionQueue[#DirectionQueue] ~= 'left'
      and DirectionQueue[#DirectionQueue] ~= 'right' then
    table.insert(DirectionQueue, 'left')
  elseif key == 'down'
      and DirectionQueue[#DirectionQueue] ~= 'down'
      and DirectionQueue[#DirectionQueue] ~= 'up' then
    table.insert(DirectionQueue, 'down')
  elseif key == 'up'
      and DirectionQueue[#DirectionQueue] ~= 'up'
      and DirectionQueue[#DirectionQueue] ~= 'down' then
    table.insert(DirectionQueue, 'up')
  end
end

function love.draw()
  -- first translate then scale
  love.graphics.translate(window.translateX,  window.translateY)
  love.graphics.scale(window.scale)

  -- cell size in pixels
  local cellSize = 5

  -- grey background
  love.graphics.setColor(.28, .28, .28)
  love.graphics.rectangle(
    "fill",
    0,
    0,
    window.width,
    window.height
  )

  local function drawCell(x, y)
    love.graphics.rectangle(
      'fill',
      (x - 1) * cellSize,
      (y - 1) * cellSize,
      cellSize - 1,
      cellSize - 1
    )
  end

  -- Snake Color
  for segmentIndex, segment in ipairs(SnakeSegments) do
    if SnakeAlive then
      love.graphics.setColor(.6, 1, .32)
    else
      love.graphics.setColor(.5, .5, .5)
    end
    drawCell(segment.x, segment.y)
  end

  -- Draw Food
  love.graphics.setColor(1, .3, .3)
  drawCell(FoodPosition.x, FoodPosition.y)

  -- Draw score
  love.graphics.setColor(.6, 1, .32)
  love.graphics.print({'Score: ', Score}, window.width / 2 -50, 30)

  -- Temporary
  -- display direction queue
  for directionIndex, direction in ipairs(DirectionQueue) do
    love.graphics.setColor(1, 1, 1)
    love.graphics.print(
      'directionQueue[' .. directionIndex .. ']: ' .. direction,
      15, 15 * directionIndex
    )
  end
end

function resize(w, h) -- update new tranlation and scale:
  local wl, hl = window.width, window.height -- target rendering resolution
  local scale = math.min(w/wl, h/hl)
  window.translateX, window.translateY, window.scale = (w-wl*scale)/2, (h-hl*scale)/2, scale
end

function love.resize(w, h)
  resize(w, h) -- update new translation and scale
end
